<script>

	/*	Watchlists Admin 1779278315 Footer 
	
		Code Status 
			- document and window parameters were used for early testing; may not be necessary
			- basic testing should be completed before document and window parameters are removed
			
		SIMS List, SQL Functions 
			- None Applicable
			
		Deployment Notes 
			- None Applicable
			
		Work Completed, By
			- 2016-03-11, By Aaron Harvey

	*/
	
	var watchlistControl = (function($,document,window){
	 
		var stopWatchlist = function(id) {
			com.gts.sim.invokeSim("SocialWL_Update_Watchlist_Status_Inactive",[{"name":"id","value":id}],function(data){},function(err){});
		};
		
		var startWatchlist = function(id) {
			com.gts.sim.invokeSim("SocialWL_Update_Watchlist_Status_Active",[{"name":"id","value":id}],function(data){},function(err){});
		};
		
		var stop = function(id){		
			return new Promise(function(resolve,reject){
				stopWatchlist(id,resolve,reject);
			});
		};
		
		var start = function(id){
			return new Promise(function(resolve,reject){
				startWatchlist(id,resolve,reject);
			});
		};
	
		var tog = function(id,status){
			status==='NO' ? start(id) : stop(id);		
		};
		
		var toggle = function(id,status){
			return new Promise(function(resolve,reject){
				tog(id,status,resolve,reject);
			});			
		}
	
		// Make toggle method visible
		return {
			toggle: toggle		
		};
	
	})(jQuery3,document,window)
</script>