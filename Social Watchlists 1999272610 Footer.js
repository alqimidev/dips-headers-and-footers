	<!-- 
		
		/** Identifier Info **/
		- Footer Code for DIP with Grid ID 1999272610, DIP Configuration Name: Social Watchlists
		- Social Watchlists 1999272610 Footer.js
		
		/** Code Notes **/
		- Code formatting may need improvement 
		- Further optimization could be completed
		- Some redundant code could be cleaned up
		- Minify for deployment ?
		
		/** SIMS List, SQL Functions **/
		SIMS List, SQL Function if any
		1 - SocialWL_Watchlist_Data.json (Standard Sim Query)
		2 - SocialWL_Watchlists_Subscribe_Form_Data.json (SQL Function: watchlist_subscribe_form_populate)
		3 - addNewwatchlist.do (Standard Sim Query)
		4 - SocialWL_Watchlist_Count.json (Standard Sim Query)
		5 - SocialWL_Watchlist_Change, SQL Function (SQL Function: watchlist_change)
		6 - SocialWL_Watchlists_Subscribe_Form_Input (SQL Function: watchlist_subscribe_submit_two)
		
		/** Deployment Notes **/
		1 - Anywhere you find .replace(/Grid_View_Form/,'8'); in the code, some configuration may be required for deployment to other portal servers. 
		2 - Make sure you have the correct "GRID_ID" variable configured (Search this document for GRID_ID)
		3 - var dropDownId may need to be double-checked if any errors arise that are associated with the drop-down selector
		
	-->
					<script src="/vendor/bacon/bacon.min.js"></script>
					<script src="/vendor/moment/moment.min.js"></script>
					<script src="/vendor/lodash/lodash.compat.min.js"></script>
					<script src="/vendor/lodash/underscore.string.min.js"></script>
					<link rel="stylesheet" href="/vendor/jstree/themes/default/style.min.css"/>
					<link rel="stylesheet" href="/vendor/jqueryui/jquery-ui-1.10.4.custom.min.css"/>
					<link rel="stylesheet" href="/vendor/jqueryui/jquery-ui-timepicker-addon.css"/>
					<link rel="stylesheet" type="text/css" href="/FIT/css/general.css">
					
					<!----------------- Form Template: Watchlist Add New --------------------------------->
					<script type="text/x-handlebars-template" id="watchlist-add">
					<form id = 'wl-add'>
						<table style="width:400px" border="0" class="ft-fit">
							<tbody>
							
							<!-- Header Row -->
							<tr>
								<td class="fs-fit" colspan="2">Create New Watchlist</td>
								</tr>
								
							<!-- Name Row -->
							<tr><td class="fl-fit">Name<span id="wl-add-name" style="color:red;font-weight:bold"> *</span></td>
								
							<!-- Text Area for Name -->
							<td class="fv-fit"><input title="" type="text" name="watchlist-name" id="watchlist-name" value=""/></td>
							</tr>
							
							<!-- Start Date -->
							<tr><td class="fl-fit">Start Date<span id="wl-add-start" style="color:red;font-weight:bold"> *</span></td>
								
							<!-- Text Area for Start Date -->
							<td class="fv-fit"><input title="" type="text" name="watchlist-start" id="watchlist-s" value=""/></td>
							</tr>
							
							<!-- End Date -->
							<tr><td class="fl-fit">End Date<span id="wl-add-end" style="color:red;font-weight:bold"> *</span></td>
								
							<!-- Text Area for End Date -->
							<td class="fv-fit"><input title="" type="text" name="watchlist-end" id="watchlist-e" value=""/></td>
							</tr>
							
							<!-- Max Hits -->
							<tr><td class="fl-fit">Max Hits<span id="wl-add-hits" style="color:red;font-weight:bold"> *</span></td>
								
							<!-- Text Area for Max Hits -->
							<td class="fv-fit"><input title="" type="text" name="watchlist-hits" id="watchlist-hits" value=""/></td>
							</tr>
							
							<!-- Geo -->
							<tr><td class="fl-fit">Geofence</td>
								
							<!-- Text Area, Globe Button for Geo -->
							<td class="fv-fit"><input title="" type="text" name="watchlist-geo" id="watchlist-geo" readonly/><span style="padding-left:10px"><input type="image" align="middle" src="/circlemap/globe-icon.png" width="20px"height="20px" alt="Geo" name="watchlist-add-geo" id="watchlist-add-geo" value="Edit Selected" title="Edit Geo" style="display:inline-block;vertical-align:middle;margin-left:auto;margin-right:auto;"
							
							/></span>
							
							<!-- Terms / Query -->
							<tr>
							<td class="fl-fit">Query</td>
								
							<!-- Text Area for Terms -->
							<td class="fv-fit"><textarea name="watchlist-terms" id="watchlist-terms" value="" cols="31" rows="6"/></td>
							</tr>
							  
							<!-- Button - Submit Row -->
							<tr><td class="fl-fit"><span id="sessionTips" align="left"><strong style="color:red">*</strong> Required</span></td>
								<!-- Buttons -->
								<td style="text-align:right !important" class="fv-fit" >
								  <input type="submit" name="confirm" id="testing" value="Save"/> <input name="cancel" type="submit" id="cancel" value="Cancel"/>
								  </td>
							  </tr>		
					  </tbody>
					</table>
					</form>
					</script>
					
					
					<!----------------------------- Form Template: Watchlist View ---------------------------->
					<script type="text/x-handlebars-template" id="watchlist-view-form">
					<form id = 'wl-view'>
						<table style="width:400px" border="0" class="ft-fit">
							<tbody>
							
							<!-- Header Row -->
							<tr>
								<td class="fs-fit" colspan="2">Watchlist Details</td>
								</tr>
								
							<!-- Name Row -->
							<tr><td class="fl-fit">Name</td>
								
							<!-- Text Area for Name -->
							<td class="fv-fit"><input title="" type="text" name="watchlist-name" id="watchlist-name" readonly/></td>
							</tr>
							
							
							<!-- Start Date -->
							<tr>
							<td class="fl-fit">Start Date<span id="wl-change-start" style="color:red;font-weight:bold"></span></td>
								
							<!-- Text Area for End Date -->
							<td class="fv-fit"><input title="" type="text" name="watchlist-start" id="watchlist-start" readonly/></td>
							</tr>
							
							
							<!-- End Date -->
							<tr>
							<td class="fl-fit">End Date<span id="wl-change-end" style="color:red;font-weight:bold"></span></td>
								
							<!-- Text Area for End Date -->
							<td class="fv-fit"><input title="" type="text" name="watchlist-end" id="watchlist-end" readonly/></td>
							</tr>
							
							
							<!-- Max Hits -->
							<tr>
							<td class="fl-fit">Max Hits<span id="wl-change-hits" style="color:red;font-weight:bold"></span></td>
								
							<!-- Text Area for Max Hits -->
							<td class="fv-fit"><input title="" type="text" name="watchlist-hits" id="watchlist-hits" readonly/></td>
							</tr>
							
							
							<!-- Geo -->
							<tr>
							<td class="fl-fit">Geofence</td>
								
							<!-- Text Area, Globe Button for Geo -->
							<td class="fv-fit"><input title="" type="text" name="watchlist-geo-text" id="watchlist-geo" readonly/>
							
							<!-- Terms / Query -->
							<tr>
							<td class="fl-fit">Query</td>
								
							<!-- Text Area for Terms / Query -->
							<td class="fv-fit"><textarea name="watchlist-terms" id="watchlist-terms" value="" cols="31" rows="6" readonly/></td>
							</tr>
							  
							<!-- Button - Submit Row -->
							<tr><td class="fl-fit"><span id="sessionTips" align="left"></span></td>
								<!-- Buttons -->
								<td style="text-align:right !important" class="fv-fit" >
								  <input name="cancel" type="submit" id="cancel" value="Cancel"/>
								</td>
							</tr>		
							  
					  </tbody>
					</table>
					</form>
					</script>
					
					
					
					<!----------------------------- Form Template: Watchlist Change (Edit) ---------------------------->
					<script type="text/x-handlebars-template" id="watchlist-change">
					<form id = 'wl-chg'>
						<table style="width:400px" border="0" class="ft-fit">
							<tbody>
							
							<!-- Header Row -->
							<tr>
								<td class="fs-fit" colspan="2">Edit Selected Watchlist</td>
								</tr>
								
							<!-- Name Row -->
							<tr><td class="fl-fit">Name</td>
								
							<!-- Text Area for Name -->
							<td class="fv-fit"><input title="" type="text" name="watchlist-name" id="watchlist-name" value="" readonly/></td>
							</tr>
							
							
							<!-- Start Date -->
							<tr>
							<td class="fl-fit">Start Date<span id="wl-change-start" style="color:red;font-weight:bold"> *</span></td>
								
							<!-- Text Area for End Date -->
							<td class="fv-fit"><input title="" type="text" name="watchlist-start" id="watchlist-start"/></td>
							</tr>
							
							
							<!-- End Date -->
							<tr>
							<td class="fl-fit">End Date<span id="wl-change-end" style="color:red;font-weight:bold"> *</span></td>
								
							<!-- Text Area for End Date -->
							<td class="fv-fit"><input title="" type="text" name="watchlist-end" id="watchlist-end"/></td>
							</tr>
							
							
							<!-- Max Hits -->
							<tr>
							<td class="fl-fit">Max Hits<span id="wl-change-hits" style="color:red;font-weight:bold"> *</span></td>
								
							<!-- Text Area for Max Hits -->
							<td class="fv-fit"><input title="" type="text" name="watchlist-hits" id="watchlist-hits" value=""/></td>
							</tr>
							
							
							<!-- Geo -->
							<tr>
							<td class="fl-fit">Geofence</td>
								
							<!-- Text Area, Globe Button for Geo -->
							<td class="fv-fit"><input title="" type="text" name="watchlist-geo-text" id="watchlist-geo" /><span style="padding-left:10px"><img align="middle" src="/circlemap/globe-icon.png" width="20px" height="20px" alt="Geo" name="watchlist-change-geo" id="watchlist-change-geo" value="Edit Selected" title="Edit Geo" style="display:inline-block;vertical-align:middle;margin-left:auto;margin-right:auto;cursor:pointer;"/></span>
							
							<!-- Terms / Query -->
							<tr>
							<td class="fl-fit">Query</td>
								
							<!-- Text Area for Terms / Query -->
							<td class="fv-fit"><textarea name="watchlist-terms" id="watchlist-terms" value="" cols="31" rows="6"/></td>
							</tr>
							  
							<!-- Button - Submit Row -->
							<tr><td class="fl-fit"><span id="sessionTips" align="left"><strong style="color:red">*</strong> Required</span></td>
								<!-- Buttons -->
								<td style="text-align:right !important" class="fv-fit" >
								  <input type="submit" name="confirm" id="<%- data.id %>" value="Save"/> <input name="cancel" type="submit" id="cancel" value="Cancel"/>
								  </td>
							</tr>		
							  
					  </tbody>
					</table>
					</form>
					</script>
					   
					<!----------------- Form Template: Watchlist Subscribe --------------------------------->
					<script type="text/x-handlebars-template" id="watchlist-subscribe">
					<form id = 'wl-subs'>
					<table border="0" style="width:480px" class="ft-fit">
						<tbody>
							<!-- Header Row -->
							<tr>
								<td class="fs-fit" colspan="2">Subscribe Watchlist</td>
								</tr>
								
							<!-- Watchlist Name Row -->
							<tr><td class="fl-fit">Name</td>
								
							<!-- Text Area for Name -->
							<td class="fv-fit"><input title="" type="text" name="watchlist-name" id="watchlist-name" readonly/></td>
							</tr>
							
							<!-- Watchlist Start Date -->
							<tr>
							<td class="fl-fit">Watchlist Start</td>
								
							<!-- Text Area for Start Date -->
							<td class="fv-fit"><input title="" type="text" name="watchlist-start" id="watchlist-start" readonly/></td>
							</tr>
							
							<!-- Watchlist End Date -->
							<tr>
							<td class="fl-fit">Watchlist End</td>
								
							<!-- Text Area for End Date -->
							<td class="fv-fit"><input title="" type="text" name="watchlist-end" id="watchlist-end" readonly/></td>
							</tr>
							
							<!-- Geo -->
							<tr> 
							<td class="fl-fit">Geofence</td>
								
							<!-- Text Area, Globe Button for Geo -->
							<td class="fv-fit"><input title="" type="text" name="watchlist-geo" id="watchlist-geo" readonly/>
							
							</td>
							</tr>
							
							<!-- Terms / Query -->
							<tr>
							<td class="fl-fit">Query</td>
								
							<!-- Text Area for Terms -->
							<td class="fv-fit"><textarea name="watchlist-terms" id="watchlist-terms" value="" cols="31" rows="6" readonly/></td>
							</tr>
							
							<!-- Subscription Types -->
							<tr>
								<td class="fl-fit">Subscriptions</td>
								<td class="fv-fit">
									<table>
									<tr><td>Daily Summary</td><td><input type="checkbox" name="check-box-daily-summary" id="check-box-daily-summary"/></td><p></p></tr>
									<tr><td>Email Alert</td><td><input type="checkbox" name="checkbox-email-alert" id="check-box-email-alert"/></td><p></p></tr>
									</table>
								</td>
							</tr>
							
							<!-- Subscription - Daily Summary End Date -->
							<tr>
							<td class="fl-fit">Daily Summary End Date</td>
								
							<!-- Text Area for End Date -->
							<td class="fv-fit"><input title="" type="text" name="subscription-ds-end" id="subscription-ds-end"/></td>
							</tr>
							
							<!-- Subscription - Email Alerts End Date -->
							<tr>
							<td class="fl-fit">Email Alerts End Date</td>
								
							<!-- Text Area for End Date -->
							<td class="fv-fit"><input title="" type="text" name="subscription-ea-end" id="subscription-ea-end"/></td>
							</tr>
							
							<!-- Button - Submit Row -->
							<tr><td class="fl-fit"></td>
								<!-- Buttons -->
								<td style="text-align:right !important" class="fv-fit" >
								  <input type="submit" name="confirm" id="testing" value="Save"/> <input name="cancel" type="submit" id="cancel" value="Cancel"/>
								  </td>
							  </tr>		
					  </tbody>
					</table>
					</form>
				</script>
					
				<script>
					/* ---------------------- Namespace Boilerplate ----------------------------------------*/
					window.top.dipsuiteEnsureNamespace = window.top.dipsuiteEnsureNamespace || function (requiredNamespace) {
						var namespacesToBeVerified = requiredNamespace.split('.'),
						namespacePart = namespacesToBeVerified.shift(),
						currentLevelElement = window.top[namespacePart] || {};
						window.top[namespacePart] = currentLevelElement;
						while (namespacesToBeVerified.length > 0) {
							namespacePart = namespacesToBeVerified.shift();
							currentLevelElement[namespacePart] = currentLevelElement[namespacePart] || {};
							currentLevelElement = currentLevelElement[namespacePart];
						}
						return currentLevelElement;
					};
					
					/* ---------------- Events Boilerplate -------------------------------------------------*/
					(function (dipSuiteGlobal) {
						dipSuiteGlobal.events = dipSuiteGlobal.events || {
							eventsMap: {},
							bind: function (eventType, callBack) {
								if (!this.eventsMap[eventType]) {
									this.eventsMap[eventType] = [];
								}
								this.eventsMap[eventType].push(callBack);
							},
							trigger: function (eventType, eventDetails) {
								if (!this.eventsMap[eventType]) {
									return false;
								}
								var self = this;
								for (var i = 0; i < this.eventsMap[eventType].length; i++) {
									(function (eventType, i, eventDetails) {
										setTimeout(function () {
											self.eventsMap[eventType][i](eventDetails);
										}, 0);
									})(eventType, i, eventDetails);
								}
								return true;
							}
						};
					})(window.top.dipsuiteEnsureNamespace('dip.suite.global'));
					
					/* --------------------- Start Forms --------------------------------------------------- */
					$(window).load(function(){ 	
					
						/**** Forms Module ****/
						(function($,_){
							
							// Constants			
							var GRID_ID = 1999272610;   /* CONFIGURABLE - This must be the Grid ID from the DIP Configuration Manager */
							var MIN_HITS = 1;
							var MAX_HITS = 150000;
						
							// Form - Generic
							var Form = function (options) {
								var $templateElement = _.isString(options.template) ? $(options.template) : options.template;
								this.template = _.template($templateElement.html());
								this.action = options.action;							                            
							};
							
							_.extend(Form.prototype, {
								formUniqueClassname: 'siftSetupForm',
								_prepareFormDiv: function _prepareFormDiv(position) {
									this.$formDiv = $('<div class="' + this.formUniqueClassname + '"></div>');
									this.$formDiv.css({
										position: 'absolute',
										zIndex: 100,
										left: position.x,
										top: position.y
									});
								},
								_closeFormDiv: function _closeFormDiv() {
									this.$formDiv.remove();
								},
								_showLoadingDiv: function _showLoadingDiv() {
									var loadingDiv = $('<div></div>');
									loadingDiv.css({
										position: 'absolute',
										top: 0,
										left: 0,
										width: this.$formDiv.width(),
										height: this.$formDiv.height(),
										zIndex: 2									
									});
									this.$formDiv.append(loadingDiv);
								},
								
								_submitForm: function _submitForm(data, formType, GRID_ID) {
									
									var self = this,
									
									formSubmitted = function () {
										
										self._closeFormDiv();
										
										// If new watchlist, add watchlist's name to drop-down list after form submits ...
										if (formType === "new"){
										
											// Find Drop-Down Filter id via GRID_ID configurable constant
											var x = document.getElementsByClassName(""+GRID_ID+"");
											var dropDownId = x[0].children[0].id.replace(/Grid_View_Form/,'8');
											
											// Get the options values in the drop-down
											var dropDown = document.getElementById(dropDownId);
											var values = [];
											
											// Push option values into values array
											for (i=0;i<dropDown.length;i++){
												values.push(dropDown[i].value);										
											}
											
											// Insert the new list's name to the values array and sort 
											values.push(data[0].value);
											values.sort(function(a, b){
												var vA = a.toLowerCase(); 
												var vB = b.toLowerCase();
												return vA === vB ? 0 : vA < vB ? -1 : 1;											
											});
											
											// Set the values via the sorted array
											for (i=0;i<(values.length-1);i++){
												
												if (values[i] !== dropDown[i].value){	
													document.getElementById(dropDownId)[i].value = values[i];
													document.getElementById(dropDownId)[i].text = values[i]; 
												}												
											}
											
											// Add the drop-down list's end element, because you're one short
											var lastValue = values[values.length-1];
											var endOption = document.createElement('option');
											
											endOption.text = lastValue;
											endOption.setAttribute('value', lastValue);
											
											document.getElementById(dropDownId).appendChild(endOption);										
										}
										
									};
									
									this._showLoadingDiv();
									
									$.when(executeUsingSim({
										parameters: data,
										simName: this.formSimExecutor
									})).then(formSubmitted).fail(formSubmitted);
								},
								
								show: function show(data, position) {
									
									var self = this;
									
									this._prepareFormDiv(position);
									$.when(this._processTemplateData(data)).then(function (result) {
										self.$formDiv.html(result.html);
										self._bindFormEvents(result.data);
										$(window.document.body).append(self.$formDiv);
										
										// Check Form Type, Update Relevant Fields, Based on the Form Type
										
										// New Watchlist Form
										if (data.formType === "watchlist-add"){
											
											$('input:text[id=watchlist-name]').val(data.watchlistName);
											
											// Watchlist Begin ...
											$('#watchlist-s').datepicker({
												dateFormat: 'yy-mm-dd', defaultDate: new Date()
											});
											
											// Watchlist End ...
											$('#watchlist-e').datepicker({
												dateFormat: 'yy-mm-dd', defaultDate: new Date()
											});
										}
										
										else if (data.formType === "watchlist-viewing"){
											
											var wlData;
											var wl_name = data.id;
											com.gts.sim.invokeSim("SocialWL_Watchlist_Data.json",[{"name":"wl_name","value":wl_name}],function(d) {
												// If Data Returned
												if (d.trim()) {
													
													wlData = $.parseJSON(d);  	// Returns array wlData with the watchlist data in element [0]
													
													// Set Form Values
													$('input[id=watchlist-name]').val(wl_name);
													$('input[id=watchlist-start]').val(wlData[0].create_dt);
													$('input[id=watchlist-end]').val(wlData[0].end_date);
													$('input[id=watchlist-hits]').val(wlData[0].max_hits);
													$('input[id=watchlist-geo]').val(wlData[0].geo_boundary);
													$('textarea#watchlist-terms').val(wlData[0].query_terms);						
												}						
											})	
											
											
										}
										
										// Edit Watchlist Form
										else if (data.formType === "watchlist-chg"){
											
											var wlData;
											var wl_name = data.id;
											com.gts.sim.invokeSim("SocialWL_Watchlist_Data.json",[{"name":"wl_name","value":wl_name}],function(d) {
												// If Data Returned
												if (d.trim()) {
													
													wlData = $.parseJSON(d);  	// Returns array wlData with the watchlist data in element [0]
													
													// Set Form Values
													$('input[id=watchlist-name]').val(wl_name);
													$('input[id=watchlist-start]').val(wlData[0].create_dt);
													$('input[id=watchlist-end]').val(wlData[0].end_date);
													$('input[id=watchlist-hits]').val(wlData[0].max_hits);
													$('input[id=watchlist-geo]').val(wlData[0].geo_boundary);
													$('textarea#watchlist-terms').val(wlData[0].query_terms);						
												}						
											})	
											// Daily Subscription End ...
											$('#watchlist-start').datepicker({
												dateFormat: 'yy-mm-dd', defaultDate: new Date()
											});
											
											// Email Alerts Subscription End ...
											$('#watchlist-end').datepicker({
												dateFormat: 'yy-mm-dd', defaultDate: new Date()
											});
											
										}
										// Subscribe to Watchlist Form
										else if (data.formType === "watchlist-subs"){
											
											var wlData;
											var wl_name = data.id;
											
											// Return Watchlist Data
											com.gts.sim.invokeSim("SocialWL_Watchlists_Subscribe_Form_Data.json",[{"name":"wl_name","value":wl_name}],function(d) {
												
												// If Data Returned
												if (d.trim()) {
													
													wlData = $.parseJSON(d);  	// Returns array wlData with the watchlist data in element [0]
													
													// Set Form Values - Watchlist Section
													$('input[id=watchlist-name]').val(wl_name);
													$('input[id=watchlist-start]').val(wlData[0].wl_start_date);
													$('input[id=watchlist-end]').val(wlData[0].wl_end_date);
													// $('input[id=watchlist-hits]').val(wlData[0].wl_max_hits);
													$('input[id=watchlist-geo]').val(wlData[0].wl_geo);
													$('textarea#watchlist-terms').val(wlData[0].wl_terms);		
													
													// Set Form Values - Subscription Section 									
													$('#check-box-daily-summary').prop('checked', wlData[0].subs_ds);	// Boolean
													$('#check-box-email-alert').prop( 'checked', wlData[0].subs_ea);  	// Boolean
													
													$('input[id=subscription-ds-end]').val(wlData[0].subs_ds_end_date);	// null or value
													$('input[id=subscription-ea-end]').val(wlData[0].subs_ea_end_date);		// null or value
													
												}						
											})
											
											// Daily Subscription End ...
											$('#subscription-ds-end').datepicker({
												dateFormat: 'yy-mm-dd', defaultDate: new Date()
											});
											
											// Email Alerts Subscription End ...
											$('#subscription-ea-end').datepicker({
												dateFormat: 'yy-mm-dd', defaultDate: new Date()
											});										
										}												
									}).fail(function (error) {
										console.error(error);
									});
								},
								
								_processTemplateData: function _processTemplateData(data) {
									
									var self = this,
											deferred = new $.Deferred();
									setTimeout(function () {
										deferred.resolve({
											html: self.template({data: data}),
											data: {}
										});
									}, 0);
									return deferred.promise();
								}
							}); // End Form Prototype
						
						
						/**** End Generic Form ****/
						
							/*** Functions Used by Generic Form  ***/
							var executeUsingSim = function executeUsingSim(options) {
										var deferred = new $.Deferred(),
												trySimTimeoutCalls = 0,
												trySimNamespace = function (options) {
													if (!(com && com.gts && com.gts.sim && com.gts.sim.invokeSim)) {
														if (trySimTimeoutCalls > SIM_MAX_TIMES_TRY) {
															deferred.reject('SIM Executor is not available');
															return;
														}
														trySimTimeoutCalls++;
														setTimeout($.proxy(trySimNamespace, null, options), SIM_LOADING_TIMEOUT);
													} else {
														simExecutionFunction(options);
													}
												},
												simExecutionFunction = function (options) {
													
													com.gts.sim.invokeSim(options.simName, options.parameters,
															function () {
																deferred.resolve.apply(deferred, arguments);
															},
															function () {
																deferred.reject();
															});
												};
										trySimNamespace(options);
										return deferred.promise();
							}
							
						/*** End Functions Used by Generic Form ***/
						
						
						/********************************* "Add Watchlist" Form ***************************************************/
						
						// Form Base - Add Watchlist
						var WatchlistAddForm = function(options){		
						   var $templateElement = _.isString(options.template) ? $(options.template) : options.template;
						   this.template = _.template($templateElement.html());
						   this.action = options.action;
						   this.formSimExecutor = 'addNewwatchlist.do';		   
						}
						
						// Form Base - Add Watchlist
						 _.extend(WatchlistAddForm.prototype, Form.prototype, {
								
								_bindFormEvents: function _bindFormEvents() {
									
									var self = this;
									
									// Geofence Data From Geo Selector to Add Watchlist Form
									dip.suite.global.events.bind('sift.campaign.maps.locationProvided', function (eventData) {
										self.$formDiv.find('form input[name=watchlist-geo]').val(eventData.lat.toFixed(6) + ',' + eventData.lng.toFixed(6) + ',' + eventData.radius.toFixed(6));
									}); 
									
									// Click Event on Geo Image
									this.$formDiv.find('form input[name="watchlist-add-geo"]').bind('click', function (event) {
										
										var formId = $('#watchlist-geo').closest('form').attr('id');
										var geoFormX = self.$formDiv.offset().left+document.getElementById(formId).parentElement.offsetWidth+1;
										var geoFormY = self.$formDiv.offset().top;
										
										dip.suite.global.events.trigger('sift.campaign.maps.openMap', {
											x: geoFormX,
											y: geoFormY
										});
									});
									
									this.$formDiv.find('form').submit(function () {
										
										var form = $(this),
											data = [									
												{name: 'wl_name', 		value: form.find('input[id=watchlist-name]').val()},
												{name: 'wl_start', 		value: form.find('input[id=watchlist-s]').val()},
												{name: 'wl_end', 		value: form.find('input[id=watchlist-e]').val()},
												{name: 'max_hits', 		value: form.find('input[id=watchlist-hits]').val()},
												{name: 'geo_boundary', 	value: form.find('input[id=watchlist-geo]').val()},
												{name: 'query_terms', 	value: form.find('textarea#watchlist-terms').val()}
											];
											
											
										// Reset all hint stars to red
										$('#wl-add-name').css("color","Red");
										$('#wl-add-name').text(" *");
									
										$('#wl-add-start').css("color","Red");
										$('#wl-add-start').text(" *");
										
										$('#wl-add-end').css("color","Red");
										$('#wl-add-end').text(" *");
										
										$('#wl-add-hits').css("color","Red");
										$('#wl-add-hits').text(" *");	
										
										// Validation variables ...
										var count = 1;
										var nameValid = false, dateStart = false, dateEnd = false, datesValid = false, hitsValid = false, geoTermsValid = false;
										var wl_name = form.find('input[id=watchlist-name]').val(); // Watchlist Name to Check 
										com.gts.sim.invokeSim("SocialWL_Watchlist_Count.json",[{"name":"wl_name","value":wl_name}],function(d) {
											
											// If Data Returned
											if (d.trim()) {								
												result = $.parseJSON(d);  	// Returns array wlData with the watchlist data in element [0]
												count = result[0].count;								
											}
											
											// Validate Watchlist Name
											if (count === 0 & wl_name.length > 0){
													
												nameValid = true;
												
												// Valid name, so change the name * to a LimeGreen CheckMark
												$('#wl-add-name').css("color","LimeGreen");
												$('#wl-add-name').html(" &#10004;");
													
											} else {
												
												form.find('input[id=watchlist-name]').val("");
												
												$('#wl-add-name').css("color","Red");
												$('#wl-add-name').text(" *");											
											}
											
										// Validate Hits ...
										var hits = $('#watchlist-hits').val();
										hits = hits.replace(/^0+/, '');
										$('#watchlist-hits').val(hits);
										
										var maxResultsCorrectCheck = function(hits) {
											var maxResults = parseInt(hits,10);
											return !!(maxResults >= MIN_HITS && maxResults <= MAX_HITS);
										}
										
										// Validate Hits
										if(maxResultsCorrectCheck(hits)){
											hitsValid = true;
											$('#wl-add-hits').css("color","LimeGreen");	
											$('#wl-add-hits').html(" &#10004;");											
										} else {
											form.find('input[id=watchlist-hits]').val("");
											$('#wl-add-hits').css("color","Red");
											$('#wl-add-hits').text(" *");										
										}
											
										/* Validate Watchlist Dates */
										
										// Validate Format Function
										function validateDateFormat(date) {
		
											// Pattern
											var regex = /^\d{4}\-\d{1,2}\-\d{1,2}$/;
											if(!regex.test(date)) {
												return false;
											}
											// Split, Parse
											var parts = date.split("-");
											var day = parseInt(parts[2], 10);
											var month = parseInt(parts[1], 10);
											var year = parseInt(parts[0], 10);
											// Ranges
											if(year < 1000 || year > 3000 || month == 0 || month > 12) {
												return false;
											}
											var monthDays = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];
											
											// Leap years
											if(year % 400 == 0 || (year % 100 != 0 && year % 4 == 0)) {
												monthDays[1] = 29;
											}
											
											// Finally, check days range
											return day > 0 && day <= monthDays[month - 1];
										}
												
										// Validate Date Sequence Function
										function validateDateSequence(startDate,endDate){
											'use strict';
											if (Date.parse(endDate)>=(Date.parse(startDate))){
												return true;
											}
											return false;												
										}
												
										// Validate Start Date
										var startDate = $('#watchlist-s').val();
										var validStartDate = validateDateFormat(startDate);
										
										if (validStartDate){
											dateStart = true;
											$('#wl-add-start').css("color","LimeGreen");	
											$('#wl-add-start').html(" &#10004;");											
										} else {										
											form.find('input[id=watchlist-s]').val(""); 	// Clear Form Field
											$('#wl-add-start').css("color","Red");
											$('#wl-add-start').text(" *");
										}
										
										// Validate End Date								
										var endDate = $('#watchlist-e').val();
										var validEndDate = validateDateFormat(endDate);
										
										if (validEndDate){
											dateEnd = true;
											$('#wl-add-end').css("color","LimeGreen");	
											$('#wl-add-end').html(" &#10004;");										
										} else {
											form.find('input[id=watchlist-e]').val("");		// Clear Form Field
											$('#wl-add-end').css("color","Red");
											$('#wl-add-end').text(" *");
										}
										
												
										if (validStartDate & validEndDate){
											
											datesValid = true;
											
											// Validate Date Sequence		
											var validDateSeq = validateDateSequence(startDate,endDate);
											
											if (validDateSeq){
												
												$('#wl-add-start').css("color","LimeGreen");	
												$('#wl-add-start').html(" &#10004;");	
											
												$('#wl-add-end').css("color","LimeGreen");	
												$('#wl-add-end').html(" &#10004;");	
												
												
											} else {
											
												$('#wl-add-start').css("color","Yellow");
												$('#wl-add-start').text(" *");
											
												$('#wl-add-end').css("color","Yellow");
												$('#wl-add-end').text(" *"); 
												return false;
											}
										}
									
											
										// Validate Geo and Terms ...
										if (form.find('input[id=watchlist-geo]').val().length > 0 | form.find('textarea#watchlist-terms').val().length > 0){
											geoTermsValid = true;
										}							
													
										// Fields Valid, Submit Form	
										if (nameValid & dateStart & dateEnd & datesValid & hitsValid & geoTermsValid) {
											var formType = "new";
											self._submitForm(data,formType,GRID_ID);															
										}
										
										}); // end sim
										
										return false;
									});
									
									this.$formDiv.find('form input[name="cancel"]').click(function () {
										self._closeFormDiv();
										return false;
									});
									
								} // end bindFormEvents
								
							});
							
						
							// Functions for Menu Click
							var watchlistAddForm = new WatchlistAddForm({
							
								template: '#watchlist-add'
								
							});
						
							// Form Functions
							var watchlistAdd = function(details,position){
								
								watchlistAddForm.show(details, position);
								
							};
						
						
						/************* End Add Watchlist *******************/
						
						
						/************* Start View Watchlist ****************/
						
						// Form Base - View Watchlist
						var WatchlistViewForm = function(options){		
						   var $templateElement = _.isString(options.template) ? $(options.template) : options.template;
						   this.template = _.template($templateElement.html());
						   this.action = options.action;
						   this.formSimExecutor = '';  
						}
						
						// Form Base - View Watchlist
						 _.extend(WatchlistViewForm.prototype, Form.prototype, {
								
							_bindFormEvents: function _bindFormEvents() {
									
								var self = this;
							
								this.$formDiv.find('form input[name="cancel"]').click(function () {
										self._closeFormDiv();
										return false;
									});								
								} // end bindFormEvents							
							});
							
						
							// Functions for Menu Click
							var watchlistViewForm = new WatchlistViewForm({
							
								template: '#watchlist-view-form'
								
							});
						
							// Form Functions
							var watchlistView = function(details,position){
								
								watchlistViewForm.show(details, position);
								
							};
						
						/************* End View Watchlist ******************/
						
						
						/************* Change Watchlist *******************/
						
						// Form Base - Change Watchlist
						var WatchlistChangeForm = function(options){		
						   var $templateElement = _.isString(options.template) ? $(options.template) : options.template;
						   this.template = _.template($templateElement.html());
						   this.action = options.action;
						   this.formSimExecutor = 'SocialWL_Watchlist_Change';  
						}
						
						// Form Base - Change Watchlist
						 _.extend(WatchlistChangeForm.prototype, Form.prototype, {
								
							_bindFormEvents: function _bindFormEvents() {
									
								var self = this;
							
								// Geofence Data
								dip.suite.global.events.bind('sift.campaign.maps.locationProvided', function (eventData) {
									self.$formDiv.find('form input[name=watchlist-geo-text]').val(eventData.lat.toFixed(6) + ',' + eventData.lng.toFixed(6) + ',' + eventData.radius.toFixed(6));
								}); 
								
								// Click Event on Geo Image
								this.$formDiv.find('img[name="watchlist-change-geo"]').bind('click', function (event) {
									
									var formId = $('#watchlist-geo').closest('form').attr('id');
									var geoFormX = self.$formDiv.offset().left+document.getElementById(formId).parentElement.offsetWidth+1;
									var geoFormY = self.$formDiv.offset().top;
									
									dip.suite.global.events.trigger('sift.campaign.maps.openMap', {
										x: geoFormX,
										y: geoFormY
									});
									
								});
								
								var hits = $(this).find('input[id=watchlist-hits]').val();
								var maxResultsCorrectCheck = function(hits) {
									var maxResults = parseInt(hits);
									return !!(maxResults >= this.MIN_HITS && maxResults <= this.MAX_HITS);
								}
									
								this.$formDiv.find('form').submit(function () {
									var form = $(this),
											data = [					
												{name: 'wl_name_original', 	value: form.find('input[id=watchlist-name]').val()},
												{name: 'wl_name_new', 	value: form.find('input[id=watchlist-name]').val()},
												{name: 'max_hits', 		value: form.find('input[id=watchlist-hits]').val()},
												{name: 'geo_boundary', 	value: form.find('input[id=watchlist-geo]').val()},
												{name: 'query_terms', 	value: form.find('textarea#watchlist-terms').val()}								
											];
											
												
										// Reset Start Date, End Date, Max Hits hint stars to red
										$('#wl-change-start').css("color","Red");
										$('#wl-change-start').text(" *");
										
										$('#wl-change-end').css("color","Red");
										$('#wl-change-end').text(" *");
										
										$('#wl-change-hits').css("color","Red");
										$('#wl-change-hits').text(" *");
										
										// Default Variables
										var dateStart = false, dateEnd = false, datesValid = false, hitsValid = false, geoTermsValid = false;
										
										// Validate Hits ...
										var hits = $('#watchlist-hits').val();
										hits = hits.replace(/^0+/, '');
										$('#watchlist-hits').val(hits);
										
										var maxResultsCorrectCheck = function(hits) {
											var maxResults = parseInt(hits,10);
											return !!(maxResults >= MIN_HITS && maxResults <= MAX_HITS);
										}
										
										// Validate Hits
										if(maxResultsCorrectCheck(hits)){
											hitsValid = true;
											$('#wl-change-hits').css("color","LimeGreen");	
											$('#wl-change-hits').html(" &#10004;");											
										} else {
											form.find('input[id=watchlist-hits]').val("");
											$('#wl-change-hits').css("color","Red");
											$('#wl-change-hits').text(" *");										
										}
										
										
										// Validate Format Function
										function validateDate(date) {
		
											// Pattern
											var regex = /^\d{4}\-\d{1,2}\-\d{1,2}$/;
											if(!regex.test(date)) {
												return false;
											}
											// Split, Parse
											var parts = date.split("-");
											var day = parseInt(parts[2], 10);
											var month = parseInt(parts[1], 10);
											var year = parseInt(parts[0], 10);
											// Ranges
											if(year < 1000 || year > 3000 || month == 0 || month > 12) {
												return false;
											}
											var monthDays = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];
											// Leap years
											if(year % 400 == 0 || (year % 100 != 0 && year % 4 == 0)) {
												monthDays[1] = 29;
											}
											// Finally, check days range
											return day > 0 && day <= monthDays[month - 1];
										}
												
										// Validate Date Sequence Function
										function validateDateSequence(startDate,endDate){
											'use strict';
											if (Date.parse(endDate)>=(Date.parse(startDate))){
												return true;
											}
											return false;												
										}
												
										// Validate Start Date
										var startDate = $('#watchlist-start').val();
										var validStartDate = validateDate(startDate);
										
										if (validStartDate){
											dateStart = true;
											$('#wl-change-start').css("color","LimeGreen");	
											$('#wl-change-start').html(" &#10004;");											
										} else {										
											form.find('input[id=watchlist-start]').val(""); 	// Clear Form Field
											$('#wl-change-start').css("color","Red");
											$('#wl-change-start').text(" *");
										}
										
										// Validate End Date
										var endDate = $('#watchlist-end').val();
										var validEndDate = validateDate(endDate);
										
										if (validEndDate){
											dateEnd = true;
											$('#wl-change-end').css("color","LimeGreen");	
											$('#wl-change-end').html(" &#10004;");										
										} else {
											
											form.find('input[id=watchlist-e]').val("");		// Clear Form Field
											$('#wl-change-end').css("color","Red");
											$('#wl-change-end').text(" *");
										}
										
										// Date Validation: Check Formats, then Sequence ...
										if (validStartDate & validEndDate){
											
											datesValid = true;
											
											// Validate Date Sequence		
											var validDateSeq = validateDateSequence(startDate,endDate);
											
											if (validDateSeq){
												
												$('#wl-change-start').css("color","LimeGreen");	
												$('#wl-change-start').html(" &#10004;");	
											
												$('#wl-change-end').css("color","LimeGreen");	
												$('#wl-change-end').html(" &#10004;");	
												
												
											} else {
											
												$('#wl-change-start').css("color","Yellow");
												$('#wl-change-start').text(" *");
											
												$('#wl-change-end').css("color","Yellow");
												$('#wl-change-end').text(" *"); 
												return false;
											}
										}
									
										
										
										// Validate Geo and Terms ...
										if (form.find('input[id=watchlist-geo]').val().length > 0 | form.find('textarea#watchlist-terms').val().length > 0){
											geoTermsValid = true;
										}							
													
										// Fields Valid, Submit Form	
										if (dateStart & dateEnd & datesValid & hitsValid & geoTermsValid) {
											self._submitForm(data);															
										}									
										return false;
									});
									
									this.$formDiv.find('form input[name="cancel"]').click(function () {
										self._closeFormDiv();
										return false;
									});								
								} // end bindFormEvents							
							});
							
						
							// Functions for Menu Click
							var watchlistChangeForm = new WatchlistChangeForm({
							
								template: '#watchlist-change'
								
							});
						
							// Form Functions
							var watchlistChange = function(details,position){
								
								watchlistChangeForm.show(details, position);
								
							};
						
						
						// End Change Watchlist
						
						
						/************* Subscribe Watchlist *****************/					
						var WatchlistSubscribeForm = function(options){		
							var $templateElement = _.isString(options.template) ? $(options.template) : options.template;
							this.template = _.template($templateElement.html());
							this.action = options.action;
							this.formSimExecutor = 'SocialWL_Watchlists_Subscribe_Form_Input';
						}
						
						// Form Base - Subscribe Watchlist
						 _.extend(WatchlistSubscribeForm.prototype, Form.prototype, {
							 
							_bindFormEvents: function _bindFormEvents() {
							
								var self = this;
								
								// Geo Selector
								this.$formDiv.find('form input[name="watchlist-subs-geo"]').click(function () {
									console.log("Geo Selector - Subscription");
									return false;
								});
									
									
								this.$formDiv.find('form').submit(function () {
									
									var form = $(this);
									
										data = [					
												{name: 'wl_name', value: form.find('input[id=watchlist-name]').val()},
												{name: 'subsCheckBoxDs', value: form.find('input[id=check-box-daily-summary]').prop('checked')},
												{name: 'subsCheckBoxEa', value: form.find('input[id=check-box-email-alert]').prop('checked')},
												{name: 'watchlistEndDateDs', value: form.find('input[id=subscription-ds-end]').val()},
												{name: 'watchlistEndDateEa', value: form.find('input[id=subscription-ea-end]').val()}								
											];
										
									
									// Combinations Correct
									var dsCombo = false, eaCombo = false, dsDate = false, dateSequenceDs = false, dateSequenceEa = false, dateSequence = false;
										
									// Validate Format Function
									function validateDate(date) {
		
											// Pattern
											var regex = /^\d{4}\-\d{1,2}\-\d{1,2}$/;
											if(!regex.test(date)) {
												return false;
											}
											
											// Split, Parse
											var parts = date.split("-");
											var day = parseInt(parts[2], 10);
											var month = parseInt(parts[1], 10);
											var year = parseInt(parts[0], 10);
											
											// Ranges
											if(year < 1000 || year > 3000 || month == 0 || month > 12) {
												return false;
											}
											var monthDays = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];
											
											// Leap years
											if(year % 400 == 0 || (year % 100 != 0 && year % 4 == 0)) {
												monthDays[1] = 29;
											}
											
											// Finally, check days range
											return day > 0 && day <= monthDays[month - 1];
										}
										
										function validateDateSequence(startDate,endDate){
											'use strict';
											if (Date.parse(endDate)>=(Date.parse(startDate))){
												return true;
											}
											return false;												
										}
										
										var today = new Date();
										dateSequenceDs = validateDateSequence(today,form.find('input[id=subscription-ds-end]').val());
										dateSequenceEa = validateDateSequence(today,form.find('input[id=subscription-ea-end]').val());
										
									// If Daily Summary Box is Checked and Date Sequence Ds is Correct, Daily Summary combination is correct
									if (form.find('input[id=check-box-daily-summary]').prop('checked') === true & dateSequenceDs === true){
										dsCombo = true;
									} else if (form.find('input[id=check-box-daily-summary]').prop('checked') === false & form.find('input[id=subscription-ds-end]').val().length===0){
										dsCombo=true;
									}
									
									// If Email Alerts Box is Checked and Date Sequence Ea is Correct, Email Alerts combination is correct
									if (form.find('input[id=check-box-email-alert]').prop('checked') === true & dateSequenceEa === true){
										eaCombo = true;
									} else if (form.find('input[id=check-box-email-alert]').prop('checked') === false & form.find('input[id=subscription-ea-end]').val().length===0){
										eaCombo=true;
									}
									
									// If combinations are correct, submit the form ...
									if (dsCombo & eaCombo) {
										self._submitForm(data);									
									}	
									
									return false;
								});
								
								this.$formDiv.find('form input[name="cancel"]').click(function () {
									self._closeFormDiv();
									return false;
								});
									
							} // end bindFormEvents
								
						});
							
						
							// Functions for Menu Click
							var watchlistSubscribeForm = new WatchlistSubscribeForm({
							
								template: '#watchlist-subscribe'
								
							});
						
							// Form Functions
							var watchlistSubscribe = function(details,position){
								
								watchlistSubscribeForm.show(details, position);
								
							};
							
						/************* End Subscribe Watchlist **************/
						
						
						/**** Add Forms to Namespace ****/
						 _.extend(window.top.dipsuiteEnsureNamespace('sift.organizations.forms'), {
							 
							init: function (options) {
								refreshGridFn = options.refreshGrid;
							},
								
							watchlistAdd: watchlistAdd,
							watchlistChange: watchlistChange,
							watchlistSubscribe: watchlistSubscribe,
							watchlistView: watchlistView
							
						});
							
							
							/****** Buttons *******/
							
							// "Add" Button Click ...
							$('#watchlist-new').click(function(){
								
								// Coordinates and position ...
								var FORM_OFFSET_RATIO = 1.6; // Determines the distance between the top left corner of the button clicked and the top left corner of the displayed form
								var formButtonOffset = Math.round($(this).height()*FORM_OFFSET_RATIO);
								
								var xy = $(this).position();
								var formLeftPosition = xy.left;
								var formTopPosition = xy.top+formButtonOffset;
								
								var position = {"x":formLeftPosition,"y":formTopPosition};			// Object to pass to forms.watchlistSubscribe(details, position);	
								
								var formType = "watchlist-add";
								var details = {
								   "formType": formType						 
								};
								sift.organizations.forms.watchlistAdd(details, position);									   
							});
							
							// "Change" Button Click ...
							$('#watchlist-chg').click(function(){
								
								// Coordinates and position ...
								var FORM_OFFSET_RATIO = 1.6; // Determines the distance between the top left corner of the button clicked and the top left corner of the displayed form
								var formButtonOffset = Math.round($(this).height()*FORM_OFFSET_RATIO);
								
								var xy = $(this).position();
								var formLeftPosition = xy.left;
								var formTopPosition = xy.top+formButtonOffset;
								
								var position = {"x":formLeftPosition,"y":formTopPosition};			// Object to pass to forms.watchlistSubscribe(details, position);	
								
								// Get the selected watchlist's name
								var formId = $('#watchlist-chg').closest('form').attr('id');
								var watchlistName = formId.replace(/Grid_View_Form/,'8');
								var selected = $('#'+watchlistName+' option:selected').text();
								var formType = "watchlist-chg";
								
								var details = {
									"id":selected,
									"formType": formType						 
								};
								sift.organizations.forms.watchlistChange(details, position);									   
							}); 
							
							// "Subscribe" Button Click ...
							$('#watchlist-subs').click(function(){
									
								// Coordinates and position ...
								var FORM_OFFSET_RATIO = 1.6; // Determines the distance between the top left corner of the button clicked and the top left corner of the displayed form
								var formButtonOffset = Math.round($(this).height()*FORM_OFFSET_RATIO);
								
								var xy = $(this).position();
								var formLeftPosition = xy.left;
								var formTopPosition = xy.top+formButtonOffset;
								
								var position = {"x":formLeftPosition,"y":formTopPosition};			// Object to pass to forms.watchlistSubscribe(details, position);	
								
								var formId = $('#watchlist-subs').closest('form').attr('id');
								var watchlistName = formId.replace(/Grid_View_Form/,'8');			// Get Watchlist Name from GridView Name
								var selected = $('#'+watchlistName+' option:selected').text();							
								var formType = "watchlist-subs";
								
								var details = {
									"id":selected,
									"formType": formType						 
								};
								
								sift.organizations.forms.watchlistSubscribe(details, position);								
							}); 				
							
							
							// "View" Button Click
							$('#watchlist-view').click(function(){
									
								// Coordinates and position ...
								var FORM_OFFSET_RATIO = 1.6; // Determines the distance between the top left corner of the button clicked and the top left corner of the displayed form
								var formButtonOffset = Math.round($(this).height()*FORM_OFFSET_RATIO);
								
								var xy = $(this).position();
								var formLeftPosition = xy.left;
								var formTopPosition = xy.top+formButtonOffset;
								
								var position = {"x":formLeftPosition,"y":formTopPosition};			// Object to pass to forms.watchlistSubscribe(details, position);	
								
								var formId = $('#watchlist-subs').closest('form').attr('id');
								var watchlistName = formId.replace(/Grid_View_Form/,'8');			// Get Watchlist Name from GridView Name
								var selected = $('#'+watchlistName+' option:selected').text();		// Selected Watchlist's Name						
								var formType = "watchlist-viewing";
								
								var details = {
									"id":selected,
									"formType": formType						 
								};
								
								sift.organizations.forms.watchlistView(details, position);								
							}); 				
							
					   }(jQuery3,_)) // end forms module
					   
					}); // end window.load
					</script>