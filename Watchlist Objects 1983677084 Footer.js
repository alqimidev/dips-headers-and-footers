<!-- 		
		/** CODE STATUS **/
		- Boilerplate code, commonly used as footer code with various DIPs
		
		/** SIMS List, SQL Functions **/
		- None Applicable
		
		/** Deployment Notes **/
		- None Applicable
		
		/** Latest Work Completed, By **/
		- 2016-03-11, By Aaron Harvey
		
-->

<script type="text/javascript">
    window.top.dipsuiteEnsureNamespace = window.top.dipsuiteEnsureNamespace || function (requiredNamespace) {
        var namespacesToBeVerified = requiredNamespace.split('.'),
                namespacePart = namespacesToBeVerified.shift(),
                currentLevelElement = window.top[namespacePart] || {};
        window.top[namespacePart] = currentLevelElement;
        while (namespacesToBeVerified.length > 0) {
            namespacePart = namespacesToBeVerified.shift();
            currentLevelElement[namespacePart] = currentLevelElement[namespacePart] || {};
            currentLevelElement = currentLevelElement[namespacePart];
        }
        return currentLevelElement;
    };
    (function (dipSuiteGlobal) {
        dipSuiteGlobal.events = dipSuiteGlobal.events || {
            eventsMap: {},
            bind: function (eventType, callBack) {
                if (!this.eventsMap[eventType]) {
                    this.eventsMap[eventType] = [];
                }
                this.eventsMap[eventType].push(callBack);
            },
            trigger: function (eventType, eventDetails) {
                if (!this.eventsMap[eventType]) {
                    return false;
                }
                var self = this;
                for (var i = 0; i < this.eventsMap[eventType].length; i++) {
                    (function (eventType, i, eventDetails) {
                        setTimeout(function () {
                            self.eventsMap[eventType][i](eventDetails);
                        }, 0);
                    })(eventType, i, eventDetails);
                }
                return true;
            }
        };
    })(window.top.dipsuiteEnsureNamespace('dip.suite.global'));
    (function ($, window) {
        var gts = window.gts;
        if (gts.grid.GetCurrentFiltersValues) {
            return;
        }
        gts.grid.GetCurrentFiltersValues = function (options) {
            var detectDipDiv = function (dipid, dipnr) {
                return jQuery3("div." + dipid + ":eq(" + dipnr + ")");
            };
            var detectDipNamespace = function (dipDiv) {
                var dipForm = dipDiv.find("form[id^='Grid_View_Form']"),
                        dipFormId = dipForm.attr("id");
                if (dipForm.length < 1) {
                    return undefined;
                }
                return dipFormId.substr("Grid_View_Form".length);
            };
            var dipid = options.dipid,
                    dipnr = options.dipnr && !isNaN(options.dipnr) ? options.dipnr : 0,
                    dipDivToProcess = detectDipDiv(dipid, dipnr),
                    dipNamespace = detectDipNamespace(dipDivToProcess),
                    callback = options.callback && typeof options.callback === "function" ? options.callback : function () {
                    },
                    resultsObject = {dipid: dipid, dipnr: dipnr, namespace: dipNamespace},
                    filtersDetails = options.filtersDetails || [],
                    cachedFiltersArray = gts.grid.ArrayFilters[dipNamespace],
                    filtersValues = {};
            if (!gts.grid.GridInformationKeeper[dipNamespace] || !cachedFiltersArray) {
                resultsObject.success = false;
                callback.call(this, resultsObject);
                return;
            }
            for (var i = 0; i < filtersDetails.length; i++) {
                if (filtersDetails[i].length < 1) {
                    continue;
                }
                var singleFilterUniqueId = filtersDetails[i],
                        concreteFilter = undefined;
                filtersValues[singleFilterUniqueId] = undefined;
                filtersValues[singleFilterUniqueId] = undefined;
                for (var j = 0; j < cachedFiltersArray.get_filtersQuantity(); j++) {
                    if (cachedFiltersArray.get_filtrObg(j).getFilterUniqueId() === singleFilterUniqueId) {
                        concreteFilter = cachedFiltersArray.get_filtrObg(j);
                        break;
                    }
                }
                if (concreteFilter === undefined) {
                    continue;
                }
                var filterId = concreteFilter.getID(),
                        currentFilterName = concreteFilter.getType() + filterId + dipNamespace,
                        inCookieFilterId = filterId + dipNamespace;
                switch (concreteFilter.getType()) {
                    case gts.grid.filters.constants.date:
                        filtersValues[singleFilterUniqueId] = [gts.grid.utils.getCookie(inCookieFilterId + 'fr'), gts.grid.utils.getCookie(inCookieFilterId + 'to')];
                        break;
                    case gts.grid.filters.constants.dropdown:
                    case gts.grid.filters.constants.list:
                        filtersValues[singleFilterUniqueId] = gts.grid.utils.getCookie(inCookieFilterId) && gts.grid.utils.getCookie(inCookieFilterId).split("||");
                        break;
                    case gts.grid.filters.constants.checkbox:
                    case gts.grid.filters.constants.radio:
                        filtersValues[singleFilterUniqueId] = [];
                        jQuery3("[name='" + currentFilterName + "']").each(function () {
                            var currentInputElement = jQuery3(this);
                            var cookieValue = gts.grid.utils.getCookie(currentInputElement.attr("id"));
                            if (cookieValue == "true") {
                                filtersValues[singleFilterUniqueId].push(currentInputElement.val());
                            }
                        });
                        break;
                    case gts.grid.filters.constants.text:
                    default :
                        filtersValues[singleFilterUniqueId] = [gts.grid.utils.getCookie(inCookieFilterId)];
                        break;
                }
                if (filtersValues[singleFilterUniqueId] && filtersValues[singleFilterUniqueId].filter) {
                    filtersValues[singleFilterUniqueId] = filtersValues[singleFilterUniqueId].filter(function (val) {
                        return val !== null && val !== undefined && typeof val !== 'undefined' && val.length > 0;
                    });
                }
                if (filtersValues[singleFilterUniqueId] && filtersValues[singleFilterUniqueId].length < 1) {
                    filtersValues[singleFilterUniqueId] = undefined;
                } else if (filtersValues[singleFilterUniqueId] && filtersValues[singleFilterUniqueId].length === 1) {
                    filtersValues[singleFilterUniqueId] = filtersValues[singleFilterUniqueId][0];
                }
            }
            resultsObject.success = true;
            resultsObject.filtersData = filtersValues;
            callback.call(this, resultsObject);
        };
    })(jQuery3, window);
    (function ($, window) {
        var DIP_ID = '1983677084',
                NetworkDetecting = {
                    detectNetworkIcon: function (network) {
                        switch (network.toLowerCase()) {
                            case 'twitter':
                                return changeProtocol('https://www.yourintelnet.org/images/social/Twitter-32.png');
                            case 'google':
                                return changeProtocol('https://www.yourintelnet.org/images/social/Google-32.png');
                            case 'youtube':
                                return changeProtocol('https://www.yourintelnet.org/images/social/YouTube-32.png');
                            case 'vk':
                                return changeProtocol('https://www.yourintelnet.org/images/social/vk-32.png');
                            case 'reddit':
                                return changeProtocol('https://www.yourintelnet.org/images/social/Reddit-32.png');
                            default:
                                return '';
                        }
                    },
                    detectFriendsUrl: function (network, username) {
                        switch (network.toLowerCase()) {
                            case 'twitter':
                                return 'https://twitter.com/' + username + '/following';
                            case 'google':
                                return 'https://plus.google.com/' + username;
                            case 'youtube':
                                return 'https://www.youtube.com/results?search_query=' + username;
                            case 'vk':
                                return 'http://vk.com/id' + username;
                            case 'reddit':
                                return 'http://www.reddit.com/user/' + username;
                            default:
                                return '';
                        }
                    },
                    detectFollowersUrl: function (network, username) {
                        switch (network.toLowerCase()) {
                            case 'twitter':
                                return 'https://twitter.com/' + username + '/followers';
                            case 'google':
                                return 'https://plus.google.com/' + username;
                            case 'youtube':
                                return 'https://www.youtube.com/results?search_query=' + username;
                            case 'vk':
                                return 'http://vk.com/id' + username;
                            case 'reddit':
                                return 'http://www.reddit.com/user/' + username;
                            default:
                                return '';
                        }
                    },
                    detectActivityUrl: function (network, username) {
                        switch (network.toLowerCase()) {
                            case 'twitter':
                                return 'https://twitter.com/' + username;
                            case 'google':
                                return 'https://plus.google.com/' + username + '/about';
                            case 'youtube':
                                return 'https://www.youtube.com/results?search_query=' + username;
                            case 'vk':
                                return 'http://vk.com/id' + username;
                            case 'reddit':
                                return 'http://www.reddit.com/user/' + username;
                            default:
                                return '';
                        }
                    },
                    detectProfileLink: function (network, username) {
                        switch (network.toLowerCase()) {
                            case 'twitter':
                                return 'https://twitter.com/' + username;
                            case 'google':
                                return 'https://plus.google.com/' + username + '/about';
                            case 'youtube':
                                return 'https://www.youtube.com/results?search_query=' + username;
                            case 'vk':
                                return 'http://vk.com/id' + username;
                            case 'reddit':
                                return 'http://www.reddit.com/user/' + username;
                            default:
                                return '';
                        }
                    },
                    detectProfileImageUrl: function (network, username) {
                        switch (network.toLowerCase()) {
                            default:
                                return '';
                        }
                    },
                    detectMessageLink: function (network, username) {
                        switch (network.toLowerCase()) {
                            case 'twitter':
                                return 'https://twitter.com/' + username;
                            case 'google':
                                return 'https://plus.google.com/' + username + '/about';
                            case 'youtube':
                                return 'https://www.youtube.com/results?search_query=' + username;
                            case 'vk':
                                return 'http://vk.com/id' + username;
                            case 'reddit':
                                return 'http://www.reddit.com/user/' + username;
                            default:
                                return '';
                        }
                    }
                };
        var currentNamespace = '__<!--//uniqID-->';
                var gridContainer = $("#Grid_View_Form" + currentNamespace),
                hiddenFilters = ['__sessionIdHiddenFilter', '__campaignIdHiddenFilter', '__caseIdHiddenFilter', '__organizationIdHiddenFilter'],
                bodyFilter = '__resultBodyFilter',
                userNameFilter = '__userNameFilter',
                protocol = document.location.protocol,
                isHttps = protocol === 'https:',
                triggerDataUpdated = function () {
                    dip.suite.global.events.trigger('sift.filter.results.updated', gridContainer.find('.grid-content-container'));
                },
                changeProtocol = function (url) {
                   
                    if (isHttps && url.indexOf('https:') !== 0) {
                        url = url.replace('http:', 'https:');
						 return url; // stub!
                    } else if (url.indexOf('http:') !== 0) {
                        url = url.replace('https:', 'http:');
						return url;
                    }
                    
                },
                processNetworkSpecificRecordsUpdates = function () {
                    gridContainer.find('.parsed-result').each(function () {
                        var $this = $(this);
                        var id = $this.data('id'),
                                network = $this.data('network'),
                                username = $this.data('username'),
                                networkDetails = {
                                    image: NetworkDetecting.detectNetworkIcon(network),
                                    friendsUrl: NetworkDetecting.detectFriendsUrl(network, username),
                                    followersUrl: NetworkDetecting.detectFollowersUrl(network, username),
                                    activityUrl: NetworkDetecting.detectActivityUrl(network, username)
                                };
                        $this.find('.network-image').append('<img width="28" src="' + networkDetails.image + '"/>');
                        $this.find('.friends-count')
                                .attr('href', networkDetails.friendsUrl)
                                .attr('target', '_blank');
                        $this.find('.followers-count')
                                .attr('href', networkDetails.followersUrl)
                                .attr('target', '_blank');
                        $this.find('.activity-count')
                                .attr('href', networkDetails.activityUrl)
                                .attr('target', '_blank');
                        $this.find('.select-to-report').on('click', function () {
                            var $this = $(this),
                                    $result = $this.closest('.parsed-result'),
                                    id = $result.data('id'),
                                    network = $result.data('network'),
                                    username = $result.data('username'),
                                    clone = $result.closest('.parsed-result').clone();
                            clone.find('.select-to-report').remove();
							
                            dip.suite.global.events.trigger('sift.filter.select.result', {
                                id: id,
                                username: username,
                                network: network,
                                element: clone
                            
							});
                            return false;
                        });
                    });
                },
                refreshGridWithProvidedFilter = function (eventDetails) {
                    var filterString = $.map(eventDetails.idsTree, function(nodeDetails){
                        switch (nodeDetails.type) {
                            case 'session':
                                return hiddenFilters[0] + '=' + nodeDetails.id;
                            case 'campaign':
                                return hiddenFilters[1] + '=' + nodeDetails.id;
                            case 'caseid':
                                return hiddenFilters[2] + '=' + nodeDetails.id;
                        }
                    }).join(',');
                    gts.grid.SetFilters({
                        dipid: DIP_ID,
                        filterString: filterString,
                        refreshGridOnComplete: true,
                        doClearFilters: false,
                        resetFilters: hiddenFilters});
                },
                refreshGridWithSearchForBody = function (searchFor) {
                    var filterString = bodyFilter + '=' + searchFor;
                    gts.grid.SetFilters({
                        dipid: DIP_ID,
                        filterString: filterString,
                        refreshGridOnComplete: true,
                        doClearFilters: false});
                },
                refreshGridWithSearchForUser = function (searchFor) {
                    var filterString = userNameFilter + '=*' + searchFor + '*';
                    gts.grid.SetFilters({
                        dipid: DIP_ID,
                        filterString: filterString,
                        refreshGridOnComplete: true,
                        doClearFilters: false});
                },
                triggerDataLoaded = function (namespace) {
                    var dipId = $('[id="grid_id' + namespace + '"]').val();
                    dip.suite.global.events.trigger('sift.filter.result.loaded', {dipid: dipId});
                };
        dip.suite.global.events.bind('sift.filter.results', refreshGridWithProvidedFilter);
        dip.suite.global.events.bind('sift.filter.wordle.selected', refreshGridWithSearchForBody);
        dip.suite.global.events.bind('sift.filter.username.selected', refreshGridWithSearchForUser);
		
	window.sendElementsToHeatMap = function (resultIds, messageCoordinates, campaignCoordinates, networks, textBodys, userNames, displayNames, profileImages) {
            var points = [],
                    redraw = false,
                    markerDetails = [];
            $.each(resultIds, function (index, resultId) {
                var locationInformation = messageCoordinates[index],
                        locationCoordinates = locationInformation.replace(/\[|\]/ig, '').split(','),
                        lat = locationCoordinates[0],
                        lon = locationCoordinates[1];
                if (isNaN(parseFloat(lat)) || isNaN(parseFloat(lon))) {
                    return;
                }
                points.push([lon, lat]);
                var userName = userNames[index].length > 0 ? userNames[index] : displayNames[index],
                        network = networks[index];
                markerDetails.push({
                    resultId: resultId,
                    profileLink: NetworkDetecting.detectProfileLink(network, userName),
                    profileImageUrl: profileImages[index],
                    userName: userName,
                    messageLink: NetworkDetecting.detectMessageLink(network, userName),
                    networkImage: NetworkDetecting.detectNetworkIcon(network),
                    messageText: textBodys[index],
                    friendsLink: NetworkDetecting.detectFriendsUrl(network, userName),
                    followersLink: NetworkDetecting.detectFollowersUrl(network, userName),
                    messagesLink: NetworkDetecting.detectActivityUrl(network, userName)
                });
            });
            dip.suite.global.events.trigger('sift.map.show.results', {
                points: points,
                redraw: redraw,
                markerDetails: markerDetails
            });
        };
		
		
        var once = function once(func) {
            var ran, result;
            if (!$.isFunction(func)) {
                throw new TypeError;
            }
            return function() {
                if (ran) {
                    return result;
                }
                ran = true;
                result = func.apply(this, arguments);
                // clear the `func` variable so the function may be garbage collected
                func = null;
                return result;
            };
        };
        
        $(document).on("dipTableLoaded", function (e, namespace) {
            var currentNamespace = '__<!--//uniqID-->';
            if (namespace === currentNamespace) {
                processNetworkSpecificRecordsUpdates();
                triggerDataUpdated();
                triggerDataLoaded(namespace);                
            }
        });
    })(jQuery3, window);
</script>
<style>
    .social_pre {
        max-width:700px;
        font-family: verdana, arial, tahoma, sans-serif;
        white-space: -moz-pre-wrap; /* Mozilla */
        white-space: -hp-pre-wrap; /* HP printers */
        white-space: -o-pre-wrap; /* Opera 7 */
        white-space: -pre-wrap; /* Opera 4-6 */
        white-space: pre-wrap; /* CSS 2.1 */
        white-space: pre-line; /* CSS 3 (and 2.1 as well, actually) */
        word-wrap: break-word; /* IE */
        -moz-binding: url('xbl.xml#wordwrap'); /* Firefox (using XBL) */
    }
</style>